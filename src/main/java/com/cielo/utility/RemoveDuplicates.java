package com.cielo.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RemoveDuplicates {

	public static String joblevelintrction = "D:\\Automation\\TlinkData\\Logs\\JobLevelIntractionfile.txt";
	public static String jobFamilyintrction = "D:\\Automation\\TlinkData\\Logs\\JobFamilyLevelIntractionfile.txt";
	public static String jobSubFamilyintrction = "D:\\Automation\\TlinkData\\Logs\\JobSubFamilyLevelIntractionfile.txt";
	public static String uniqueSubfamily = "D:\\Automation\\TlinkData\\Logs\\UniqueSubFamily.txt";
	public static String Grade = "D:\\Automation\\TlinkData\\Logs\\Grade.txt";
	public static String inputFile = "D://Automation//TlinkData//Logs//InputFIleLink.txt";

	public static List uniquevalue = new ArrayList<String>();
	public static List gradevalue = new ArrayList<String>();
	public static void main(String[] args) throws Exception {

		String Environment = "";
		if (args.length > 0) {
			Environment = args[0];
		}
		// TODO Auto-generated method stub

		if (Environment.equals("P")) {
			joblevelintrction = "C:\\Users\\daniel.sessions\\Automation\\TlinkData\\Logs\\JobLevelIntractionfile.txt";
			jobFamilyintrction = "C:\\Users\\daniel.sessions\\Automation\\TlinkData\\Logs\\JobFamilyLevelIntractionfile.txt";
			jobSubFamilyintrction = "C:\\Users\\daniel.sessions\\Automation\\TlinkData\\Logs\\JobSubFamilyLevelIntractionfile.txt";
			uniqueSubfamily = "C:\\Users\\daniel.sessions\\Automation\\TlinkData\\Logs\\UniqueSubFamily.txt";
			inputFile = "C://Users//daniel.sessions//Automation//TlinkData//Logs//InputFIleLink.txt";
			Grade = "C:\\Users\\daniel.sessions\\Automation\\TlinkData\\Logs\\Grade.txt";

		}
		try {
			filedelete(uniqueSubfamily);
			filedelete(jobSubFamilyintrction);
			filedelete(jobFamilyintrction);
			filedelete(joblevelintrction);
			filedelete(Grade);
			joblevel();
			jobFamily();

			Path filePath = Paths.get(inputFile);
			StringBuffer sBuffer = new StringBuffer();
			StringBuffer sBufferGrade = new StringBuffer();
			Charset charset = StandardCharsets.UTF_8;

			List<String> lines = Files.readAllLines(filePath, charset);
			for (String line : lines) {

				String[] locallist = line.split(",");
				System.out.println(line);
				if (!uniquevalue.contains(locallist[2])) {
					uniquevalue.add(locallist[2]);
					sBuffer.append(locallist[2]);
					sBuffer.append("\n");
				}
				
				if (!gradevalue.contains(locallist[4])) {
					gradevalue.add(locallist[4]);
					sBufferGrade.append(locallist[4]);
					sBufferGrade.append("\n");
				}
				

			}

			FileWriter writer = new FileWriter(uniqueSubfamily, true);
			writer.write(sBuffer.toString());
			writer.close();
			
			writer = new FileWriter(Grade, true);
			writer.write(sBufferGrade.toString());
			writer.close();

		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("Failure");
		}
		System.out.println("Success");
	}

	public static void joblevel() {

		Map<String, Set<String>> joblevel = new HashMap<String, Set<String>>();

		Path filePath = Paths.get(inputFile);
		StringBuffer sBuffer = new StringBuffer();

		Charset charset = StandardCharsets.UTF_8;
		try {
			List<String> lines = Files.readAllLines(filePath, charset);
			for (String line : lines) {

				String[] locallist = line.split(",");

				if (!joblevel.containsKey(locallist[0])) {
					Set<String> jobfamilylist = new HashSet<String>();
					jobfamilylist.add(locallist[1].trim());
					joblevel.put(locallist[0], jobfamilylist);
				} else {
					joblevel.get(locallist[0]).add(locallist[1].trim());

				}

			}
			filewriter(joblevelintrction, joblevel);
			replace(joblevelintrction);

		} catch (Exception ex) {

		}
	}

	public static void jobFamily() {

		Map<String, Set<String>> jobfamily = new HashMap<String, Set<String>>();
		Map<String, Set<String>> jobsubfamily = new HashMap<String, Set<String>>();

		Path filePath = Paths.get(inputFile);
		StringBuffer sBuffer = new StringBuffer();

		Charset charset = StandardCharsets.UTF_8;
		try {
			List<String> lines = Files.readAllLines(filePath, charset);
			for (String line : lines) {

				String[] locallist = line.split(",");

				if (!jobfamily.containsKey(locallist[1])) {
					Set<String> jobfamilylist = new HashSet<String>();
					jobfamilylist.add(locallist[2]);
					jobfamily.put(locallist[1], jobfamilylist);
				} else {
					jobfamily.get(locallist[1]).add(locallist[2]);

				}

			}
			filewriter(jobFamilyintrction, jobfamily);
			replace(jobFamilyintrction);

		} catch (Exception ex) {

		}

		try {
			List<String> lines = Files.readAllLines(filePath, charset);
			for (String line : lines) {

				String[] locallist = line.split(",");

				if (!jobsubfamily.containsKey(locallist[2])) {
					Set<String> jobsubfamilylist = new HashSet<String>();
					jobsubfamilylist.add(locallist[3]);
					jobsubfamily.put(locallist[2], jobsubfamilylist);
				} else {
					jobsubfamily.get(locallist[2]).add(locallist[3]);

				}

			}
			filewriter(jobSubFamilyintrction, jobsubfamily);
			replace(jobSubFamilyintrction);

		} catch (Exception ex) {
			ex.printStackTrace();

		}
	}

	public static void replace(String filepath) {

		String originalFilePath = filepath;
		String originalFileContent = "";

		BufferedReader reader = null;
		BufferedWriter writer = null;

		try {

			reader = new BufferedReader(new FileReader(originalFilePath));
			String currentReadingLine = reader.readLine();
			while (currentReadingLine != null) {
				originalFileContent += currentReadingLine + System.lineSeparator();
				currentReadingLine = reader.readLine();
			}
			String modifiedFileContent = originalFileContent.replaceAll("\\[", "");
			modifiedFileContent = modifiedFileContent.replaceAll("\\]", "");
			writer = new BufferedWriter(new FileWriter(originalFilePath));
			writer.write(modifiedFileContent);

		} catch (IOException e) {
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}

				if (writer != null) {
					writer.close();
				}

			} catch (IOException e) {
			}
		}
	}

	public static void filewriter(String filename, Map<String, Set<String>> joblevel) throws Exception {
		FileWriter writer = new FileWriter(filename, true);
		StringBuffer sBuffer = new StringBuffer();

		joblevel.forEach((k, v) -> {
			try {
				writer.write(k.toString() + "," + v.toString().trim());
				writer.write("\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		);

		writer.write(sBuffer.toString());
		writer.close();

	}

	public static void filedelete(String filepath) {

		Path file = Paths.get(filepath);

		try {
			Files.deleteIfExists(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
