package com.cielo.utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;

import com.cielo.Sqlite.DBConnection;

public class BackGroundFileReader {

	public static String FilePath = "D:\\Automation\\DBMigration\\BackGround_Verification_Consolidated_Report_2020.10.csv";
	private static boolean headercheck = false;

	public static void main(String[] args) throws Exception, IOException {

		if (args.length > 0) {
			FilePath = args[0];
		}
		String line = "";
		BufferedReader breader = new BufferedReader(new FileReader(FilePath));

		Connection connection = DBConnection.openConnection("BackGround_Verification.db");

		while ((line = breader.readLine()) != null) {
			if (headercheck) {
				DBConnection.insertValue(connection, line);
			}
			headercheck = true;
		}

		DBConnection.selectValue(connection, "MasterDB");

		if (!connection.isClosed()) {
			connection.close();
			System.out.println("closed");
		}

	}

}
