package com.cielo.Sqlite;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.ResourceBundle;

public class DBConnection {
	public static ResourceBundle prop=ResourceBundle.getBundle("Query");

	public static void main(String[] args) throws Exception {

		/* @param fileName the database file name -create DB */

//		try {
//			createNewDatabase("BackGround_Verification.db");
//		} catch (ClassNotFoundException e1) {
//			e1.printStackTrace();
		
//		}
		//Boolean result= createTable(connection);
		Connection connection = openConnection("BackGround_Verification.db");

		if (!connection.isClosed()) {
			connection.close();
			System.out.println("closed");
		}
	}

	/* @param fileName the database file name */
	public static void createNewDatabase(String fileName) throws ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		String url = "jdbc:sqlite:" + fileName;

		try (Connection conn = DriverManager.getConnection(url)) {
			if (conn != null) {
				DatabaseMetaData meta = conn.getMetaData();
				System.out.println("The driver name is " + meta.getDriverName());
				System.out.println("A new database" + fileName + " has been created.");
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public static Connection openConnection(String DbName) {
		Connection connection = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + DbName);
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);

		}
		System.out.println("Opened database successfully");

		return connection;

	}

	public static Boolean createTable(Connection connection) {
		
		Boolean execution =true;
		
		String table = "CREATE TABLE MasterDB (\r\n" + "	TimeStamp TEXT ,\r\n" + "	AccountName TEXT NOT NULL,\r\n"
				+ "	 First_Name TEXT NOT NULL,\r\n" + "	Last_Name TEXT NOT NULL UNIQUE,\r\n"
				+ "	Email TEXT NOT NULL,\r\n" + "	Case_Number TEXT NOT NULL,\r\n"
				+ "	Owner_Alis TEXT NOT NULL,\r\n" + "	Case_Start_Time TEXT NOT NULL,\r\n"
				+ "	Case_End_Time TEXT NOT NULL,\r\n" + "	Total_Case_Time TEXT NOT NULL,\r\n" +
				"	Record_Result TEXT NOT NULL,\r\n" + "	Result_Message TEXT,\r\n"
				+ "	Total_Records_Updated TEXT NOT NULL,\r\n" + "	Updated_Alert_Count TEXT NOT NULL,\r\n"
				+ "	Updated_QA_Review_Count TEXT NOT NULL,\r\n" + "	Updated_Clear_Count TEXT NOT NULL\r\n" +
				");";

		try
		{
			Statement statemnet=connection.createStatement();
			statemnet.execute(table);
			
		}
		catch(Exception ex)
		{
			execution=false;
			 System.out.println(ex.getMessage());
		}
		return execution;

	}
	
	public static void insertValue(Connection connection, String columnValues)
	{
		
		String[] columnVal=columnValues.split(",");
		try {
			String query=prop.getString("insert");
			PreparedStatement insertval=connection.prepareStatement(query);
			for(int i=0;i<columnVal.length;i++)
			{
				int k=i+1;
			insertval.setString(k,columnVal[i]);
			}
			
			insertval.executeUpdate();
			System.out.println("Inserted success");
			}
			catch(Exception es)
			{
				es.printStackTrace();
			}
		
		
		
		
		
	}
	
	public static void selectValue(Connection connection, String TableName) throws Exception
	{
		String query="select * from"+" "+TableName;
		
		Statement statement=connection.createStatement();
		
		ResultSet rs = statement.executeQuery(query);
	      while(rs.next()){
	         //Display values
	         System.out.print(rs.getString("AccountName"));
	         System.out.print("\r\n");
	         
	      }
	      rs.close();
	}
	
}
